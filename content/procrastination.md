+++
title = "Procrastination"
date = 2020-06-18
+++

> The abstract concept “society” means to the individual human being the sum total of his direct and indirect relations to his contemporaries and to all the people of earlier generations. The individual is able to think, feel, strive, and work by himself; but he depends so much upon society—in his physical, intellectual, and emotional existence—that it is impossible to think of him, or to understand him, outside the framework of society. It is “society” which provides man with food, clothing, a home, the tools of work, language, the forms of thought, and most of the content of thought; his life is made possible through the labor and the accomplishments of the many millions past and present who are all hidden behind the small word “society.”
>
> — <cite>Albert Einstein, Why Socialism?</cite>

The engineered society we live in teases out certain behaviors from us and procrastination is one of them. If we are to believe that a solution to this debilitating tendency exists, then we are implicitly acknowledging 1) a lack of equilibrium in the relationship between the procrastinating individual and the rest of reality with which the individual is integrated and 2) the said equilibrium can be restored or established at all.

The tasks which one procrastinates on are products of the contingent reality we inhabit and they appear to have been imposed, directly or indirectly, by any number of institutions that enforce the ways of our society. It will not be correct to lay the blame on the procrastinator. Attempting to find fixes within the individual would also be misguided because it situates the procrastination-enabling society beyond scrutiny and criticism.

The recurring unwillingness to reach a certain goal lies in one’s recognition of its contingent or imposed nature. Such recognition is had in light of one’s lack of freedom. If one were truly free, one could create their own set of choices that are 1) abstracted from all contingencies that might determine their will and 2) determined in accordance with the principles that answer to the authoritative element within them. Procrastination will fail to germinate in that case for the freely chosen and self-imposed tasks will be such that one would be willing to accomplish them.

> I am only truly free when the other is also free and is recognized by me as free.
>
> — <cite>G. W. F. Hegel, Philosophy of Mind.</cite>

In a society where all are not equally free let alone recognized by all as such, freedom remains an illusion. Our existing society—with its institutionalized asymmetric relationships—is an example of an unfree society. Most behaviors exhibited in it are not freely chosen by individuals but are mere reactions or conformances to the existing set of rules which themselves are enforced by the powerful through institutional thuggery.

With extrinsic set of rewards and punishments in place, the tasks which the masses are expected to accomplish tend to be devoid of fulfilling meaning for them. This in turn works towards draining their enthusiasm such that even for tasks which they would otherwise like to accomplish, they have little time and energy left to take them on.

With the standardizing processes in place, with institutional pushes to behave in certain ways, with clockwork precision based routine life, creativity itself is stifled on a grand scale. This inconsiderate march of events is passively resisted by the hapless individual, in part, through procrastination.

The resolution to it and many more ailments is found in absolute freedom—in the realization of anarchy.

> In this absolute freedom, therefore, all social groups or classes which are the spiritual spheres into which the whole is articulated are abolished; the individual consciousness that belonged to any such sphere, and willed and fulfilled itself in it, has put aside its limitation; its purpose is the general purpose, its language universal law, its work the universal work.
>
> — <cite>G. W. F. Hegel, The Phenomenology of Spirit.</cite>

> Anarchy is society organized without authority, meaning by authority the power to impose one’s own will and not the inevitable and beneficial fact that he who has greater understanding of, as well as ability to carry out, a task succeeds more easily in having his opinion accepted, and of acting as a guide on the particular question, for those less able than himself.
>
> — <cite>Errico Malatesta, Life and Ideas.</cite>

---
