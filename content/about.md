+++
title = "About"
date = 2020-04-04
+++

My name is Aman Agnihotri. I am an avid reader of non-fiction, particularly of works in programming, philosophy, and politics. Childhood experience with playing video games led me to pursue a course in Computer Science with an emphasis on game development. Friends made along the way got me into studying various other disciplines so I could both comprehend and contribute in their conversations.

#### Work

I currently work as a server-side programmer in a gaming company where my responsibilities include:

- designing API specifications and implementing the same for mobile game experiences;

- creating administrative tools that facilitate user support, configuration adjustments, and realization of insights;

- analyzing user data and representing it in forms that aid in subsequent design decisions;

- writing documentation to streamline client-side integration;

- managing AWS-based infrastructure and monitoring failures;

My earlier work is in client-side game and tools programming, particularly in areas of animation, gameplay, general game mechanics, and level editors.

#### Plan

Through this site, I intend to do the following:

- Showcase elegant and efficient ways of approaching problems in the programming languages I happen to know;

- Explicate the ideas of philosophers like Georg Wilhelm Friedrich Hegel and political figures like Peter Kropotkin;

- Share excerpts from various books which I deem interesting, including a list of recommendations for the same;

- Express my views on a range of topics;

Ensuring simplicity in all the content which I produce here will be my goal.

#### Contact

My email address is `amanagnihotri (at) protonmail (dot) com`.

---
